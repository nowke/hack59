import json

from django.shortcuts import render
from django.http import HttpResponse

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from dbase.models import HUser, Vendor, Solution, Location
from django.contrib.auth.models import User

from .serializers import HUserSerializser, VendorSerializer, ServicesSerializer

# Create your views here.
class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

class UserViewset(viewsets.ViewSet):
    allowed_methods = ['GET', 'POST']

    def post(self, request, format=None):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        basic = body['basic']
        user = User.objects.create_user(basic['username'], basic['username'], basic['password'])
        user.save()
        huser = HUser(user=user, name=body['name'], phone=body['phone'])
        huser.save()

        return JSONResponse({"success": True})

    def list(self, request, format=None):
        return None    

class VendorViewset(viewsets.ViewSet):
    allowed_methods = ['GET', 'POST']

    def post(self, request, format=None):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        basic = body['basic']
        user = User.objects.create_user(basic['username'], basic['username'], basic['password'])
        user.save()
        vendor = Vendor(user=user, name=body['name'], phone=body['phone'])
        vendor.save()

        return JSONResponse({"success": True})

    def list(self, request, format=None):
        return None   

class LoginView(viewsets.ViewSet):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated, )

    def list(self, request):
        user = request.user
        try:
            huser = HUser.objects.get(user=user)
            serializer = HUserSerializser(huser)
            utype = 'H'
        except HUser.DoesNotExist:
            vendor = Vendor.objects.get(user=user)
            serializer = VendorSerializer(vendor)
            utype = 'V'
        except Vendor.DoesNotExist:
            return None

        data = serializer.data
        data['utype'] = utype
        return JSONResponse(data)

class ServiceListView(viewsets.ViewSet):

    def list(self, request, format=None):
        services = Solution.objects.all()
        serializer = ServicesSerializer(services, many=True)
        return JSONResponse(serializer.data)

class ServiceSelectView(viewsets.ViewSet):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        service_ids = list(body)
        vendor = Vendor.objects.get(user=request.user)
        checked_services = []
        for service_id in service_ids:
            service = Solution.objects.get(id=service_id)
            checked_services.append(service)
        vendor.solutions = checked_services
        vendor.save()

        return JSONResponse({"success": True})

    def list(self, request, format=None):
        return None 

class VendorLocationView(viewsets.ViewSet):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        vendor = Vendor.objects.get(user=request.user)
        location = Location(latitude=body['latitude'], longitude=body['longitude'])
        location.save()
        vendor.location = location
        vendor.save()

        return JSONResponse({"success": True})

    def list(self, request, format=None):
        return Nonex        