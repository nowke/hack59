from django.contrib.auth.models import User, Group
from rest_framework import serializers
from dbase.models import HUser, Vendor, Solution, Location

class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User

class HUserSerializser(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = HUser
		fields = ('name', 'phone')

class VendorSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Vendor
		fields = ('name', 'phone')

class ServicesSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Solution
		fields = ('id', 'title')

class LocationSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Location