from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .mixins import LoginRequiredMixin, VendorRequiredMixin
from dbase.models import HUser, Vendor, Solution, Problem, Requirement, Request

def getUser(user):
	h = HUser.objects.get(user=user)
	return h

def getVendor(user):
	h = Vendor.objects.get(user=user)
	return h

# Create your views here.
class HomeView(TemplateView):
	template_name = "ui/home.html"

	def get(self, request):
		if request.user.is_authenticated and request.user.is_active:
			return redirect('ui:userdash')
		else:
			return render(request, self.template_name)

	def post(self, request):
		username = request.POST['email']
		password = request.POST['password']

		user = authenticate(username=username, password=password)
		try:
			huser = HUser.objects.get(user__username=username)
			if user is not None and user.is_active:
				login(request, user)
				return redirect('ui:userdash')

		except HUser.DoesNotExist:
			try:
				vendor = Vendor.objects.get(user__username=username)
				if user is not None and user.is_active:
					login(request, user)
					return redirect('ui:vendordash')
			except Vendor.DoesNotExist:
				pass


class UserDashView(LoginRequiredMixin, TemplateView):
	template_name = "ui/user_dash.html"

	def get(self, request):
		huser = getUser(request.user)
		solutions = Solution.objects.all()
		context = {
			"huser": huser,
			"solutions": solutions,
		}
		return render(request, self.template_name, context)

	def post(self, request):
		huser = getUser(request.user)
		if "social" in request.POST:
			solution = Solution.objects.get(id=int(request.POST['solution']))
			problem = Problem(desc=request.POST['comment'], solution=solution)
			problem.save()
			huser.problems.add(problem)
			huser.save()

			return redirect("ui:usermap", 1, solution.id, problem.id)
		elif "home" in request.POST:
			solution = Solution.objects.get(id=int(request.POST['solution_home']))
			requirement = Requirement(solution=solution, desc=request.POST['comment'])
			requirement.save()

			huser.requirements.add(requirement)
			huser.save()

			return redirect("ui:usermap", 2, solution.id, requirement.id)

class UserMapView(LoginRequiredMixin, TemplateView):
	template_name = "ui/user_map.html"

	def get(self, request, prob_type, s_id, r_id):
		solution = Solution.objects.get(id=int(s_id))
		vendors = Vendor.objects.filter(solutions__in=[solution])

		context = {
			"prob_type": int(prob_type),
			"vendors": vendors,
			"prob_id": int(r_id),
		}
		return render(request, self.template_name, context)

	def post(self, request, prob_type, s_id, r_id):
		print(request.POST.get('id'))

class PlaceRequestView(LoginRequiredMixin, TemplateView):
	template_name = "ui/place.html"

	def post(self, request, prob_type, p_id, v_id):
		if int(prob_type) == 1:
			problem = Problem.objects.get(id=int(p_id))
			vendor = Vendor.objects.get(id=int(v_id))
			huser = getUser(request.user)
			mrequest = Request(user=huser, vendor=vendor, problem=problem)
			mrequest.save()

		elif int(prob_type) == 2:
			requirement = Requirement.objects.get(id=int(p_id))
			vendor = Vendor.objects.get(id=int(v_id))
			huser = getUser(request.user)
			mrequest = Request(user=huser, vendor=vendor, requirement=requirement)
			mrequest.save()

		return render(request, self.template_name)

class LogoutView(View):
	def get(self, request):
		logout(request)
		return redirect('ui:home')		

class VendorDashboard(VendorRequiredMixin, TemplateView):
	template_name = "ui/vendor_dash.html"

	def get(self, request):
		vendor = getVendor(request.user)
		requests = Request.objects.filter(vendor=vendor)
		context = {
			"requests": requests,
		}
		return render(request, self.template_name, context)

class SignupView(TemplateView):
	template_name = "ui/signup.html"

	def post(self, request):
		name = request.POST['name']
		email = request.POST['email']
		phone = request.POST['phone']
		password = request.POST['password']
		usertype = request.POST['usertype']

		user = User.objects.create_user(email, email, password)
		user.save()

		if usertype == "vendor":
			vendor = Vendor(user=user, name=name, phone=phone)
			vendor.save()
			return redirect("ui:signup_services", vendor.id)
		else:
			huser = HUser(user=user, name=name, phone=phone)
			huser.save()

			return redirect("ui:home")

class VendorServicesView(TemplateView):
	template_name = "ui/vendor_services.html"

	def get(self, request, u_id):
		solutions = Solution.objects.all()
		context = {
			"solutions": solutions,
		}
		return render(request, self.template_name, context)

	def post(self, request, u_id):
		vendor = Vendor.objects.get(id=u_id)
		solution_ids = request.POST.getlist('solution')
		print(solution_ids)
		for sol_id in solution_ids:
			print(sol_id)
			sol = Solution.objects.get(id=sol_id)
			vendor.solutions.add(sol)
		vendor.save()

		return redirect("ui:home")

class UserProfileView(LoginRequiredMixin, TemplateView):
	template_name = "ui/user_profile.html"

	def get(self, request):
		huser = getUser(request.user)
		context = {
			"huser": huser,
		}
		return render(request, self.template_name, context)