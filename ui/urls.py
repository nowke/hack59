from django.conf.urls import include, url
from .import views

urlpatterns = [
	url(r'^$', views.HomeView.as_view(), name='home'),
	url(r'^dashboard/$', views.UserDashView.as_view(), name='userdash'),
	url(r'^dashboard/profile$', views.UserProfileView.as_view(), name='userprofile'),
	url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
	url(r'^place/(?P<prob_type>[0-9]+)/(?P<p_id>[0-9]+)/(?P<v_id>[0-9]+)/$', views.PlaceRequestView.as_view(), name='place'),
	url(r'^dashboard/map/(?P<prob_type>[0-9]+)/(?P<s_id>[0-9]+)/(?P<r_id>[0-9]+)$', views.UserMapView.as_view(), name='usermap'),

	url(r'^vdashboard/$', views.VendorDashboard.as_view(), name='vendordash'),
	url(r'^signup/$', views.SignupView.as_view(), name='signup'),
	url(r'^signup/services/(?P<u_id>[0-9]+)/$', views.VendorServicesView.as_view(), name='signup_services'),
]