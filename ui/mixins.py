from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import Http404
from django.core.urlresolvers import reverse_lazy

from dbase.models import HUser, Vendor

login_link = reverse_lazy('ui:home')

class LoginRequiredMixin(object):

	@classmethod
	def as_view(self, *args, **kwargs):
		view = super(LoginRequiredMixin, self).as_view(*args, **kwargs)
		return login_required(view, login_url=login_link)

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		try:
			customer = HUser.objects.get(user=request.user)
			return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)
		except HUser.DoesNotExist:
			raise Http404

class VendorRequiredMixin(object):
	@classmethod
	def as_view(self, *args, **kwargs):
		view = super(VendorRequiredMixin, self).as_view(*args, **kwargs)
		return login_required(view, login_url=login_link)

	@method_decorator(login_required)
	def dispatch(self, request, *args, **kwargs):
		try:
			customer = Vendor.objects.get(user=request.user)
			return super(VendorRequiredMixin, self).dispatch(request, *args, **kwargs)
		except Vendor.DoesNotExist:
			raise Http404