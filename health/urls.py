"""health URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from api import views as api_views

router = routers.DefaultRouter()
router.register(r'users', api_views.UserViewset, base_name='users')
router.register(r'vendors', api_views.VendorViewset, base_name='vendors')
router.register(r'login', api_views.LoginView, base_name='login')
router.register(r'services', api_views.ServiceListView, base_name='services')
router.register(r'service_select', api_views.ServiceSelectView, base_name='service_select')
router.register(r'vendor_loc', api_views.VendorLocationView, base_name='vendor_loc')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^', include('ui.urls', namespace='ui')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
