from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Location(models.Model):
	latitude = models.FloatField()
	longitude = models.FloatField()

	def __str__(self):
		return str(self.latitude) + ", " + str(self.longitude)

class Solution(models.Model):
	title = models.CharField(max_length=100)

	def __str__(self):
		return self.title

class Problem(models.Model):
	location = models.ForeignKey(Location, null=True)
	# title = models.CharField(max_length=10)
	desc = models.TextField()
	solution = models.ForeignKey(Solution, null=True)

class Requirement(models.Model):
	desc = models.TextField()
	location = models.ForeignKey(Location, null=True)
	solution = models.ForeignKey(Solution)

class HUser(models.Model):
	user = models.OneToOneField(User)
	name = models.CharField(max_length=50)
	phone = models.CharField(max_length=10)
	problems = models.ManyToManyField(Problem, blank=True)
	requirements = models.ManyToManyField(Requirement, blank=True)

class Vendor(models.Model):
	location = models.ForeignKey(Location, null=True)
	user = models.OneToOneField(User)
	name = models.CharField(max_length=50, blank=True)
	address = models.TextField(blank=True)
	phone = models.CharField(max_length=10, blank=True)
	solutions = models.ManyToManyField(Solution, blank=True)

class Request(models.Model):
	user = models.ForeignKey(HUser)
	vendor = models.ForeignKey(Vendor)
	problem = models.ForeignKey(Problem, blank=True, null=True)
	requirement = models.ForeignKey(Requirement, blank=True, null=True)