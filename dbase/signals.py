from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.conf import settings

from . import models

@receiver(post_save, sender=User)
def create_profile_handler(sender, instance, created, **kwargs):
	if not created:
		return
	# Create the profile object, only if it is newly created
	huser = models.HUser(user=instance)
	huser.save()