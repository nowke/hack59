from django.contrib import admin
from django.contrib.auth.models import User
from .models import Location, Solution, Problem, Requirement, HUser, Vendor, Request

# Register your models here.
class LocationAdmin(admin.ModelAdmin):
	list_display = ('latitude', 'longitude')

class SolutionAdmin(admin.ModelAdmin):
	list_display = ('title', )

class ProblemAdmin(admin.ModelAdmin):
	list_display = ('desc', )

class HUserAdmin(admin.ModelAdmin):
	list_display = ('name', 'phone',)

class VendorAdmin(admin.ModelAdmin):
	list_display = ('user',)

class RequestAdmin(admin.ModelAdmin):
	list_display = ('id',)

admin.site.register(HUser, HUserAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Solution, SolutionAdmin)
admin.site.register(Problem, ProblemAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(Request, RequestAdmin)